﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Streaming
{
    class Program
    {
        static void Main(string[] args)
        {
            string file;
            string path = @"../../../temp/";

            Console.WriteLine("Co chcesz zrobić: \n 1 - dodaj tekst do istniejącego plik \n 2 - utworzyć nowy plik \n 3 - usunąć plik \n Podaj numer:");
            int.TryParse(Console.ReadLine(), out int action);

            switch (action)
            {
                case 1:
                    {
                        getExistingFils(path);
                        Console.WriteLine("Podaj nazwę istniejącego pliku, który chcesz zmienić");
                        file = Console.ReadLine();
                        StreamWriter(path, file);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Podaj nazwę istniejącego pliku, który chcesz zmienić");
                        file = Console.ReadLine();
                        StreamWriter(path, file);
                        break;
                    }
                case 3:
                    {
                        getExistingFils(path);
                        Console.WriteLine("Podaj nazwę istniejącego pliku, który chcesz usunąć");
                        file = Console.ReadLine();
                        File.Delete(path + file);
                        break;
                    }
                default:
                    {
                        Console.Write("Brak poprawnych danych, zamknij program");
                        break; 
                    }
            }
        }

        static void getExistingFils(String path)
        {
            Console.WriteLine("Istniejące pliki:");
            string[] fileEntries = Directory.GetFiles(path);
            List<string> fileNames = new List<FileInfo>(new DirectoryInfo(@path).GetFiles("*.txt")).Select(file => file.Name).ToList();
            var result = string.Join("\n", fileNames);
            Console.WriteLine(result);
        }

        static void StreamWriter(string path, string file)
        {

            if (!file.EndsWith(".txt"))
            {
                file += ".txt";
            }
            
            Console.WriteLine("Podaj text:");
            string text = Console.ReadLine();

            using StreamWriter sr = File.AppendText(path + file);
            sr.WriteLine(text);
            sr.Close();
            Console.WriteLine("Zapisano: " + File.ReadAllText(path + file) + " do " + path + file);
        }
    }
}
